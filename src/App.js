import React, {Component} from 'react';
import classes from'./App.css';
import Person from './Person/Person';

class App extends Component {
    state = {
        persons: [
            {id: 'aas', name: 'Max', age: 28},
            {id: 'aad', name: 'Manu', age: 29},
            {id: 'aag', name: 'Stephanie', age: 26}
        ],
        otherState: 'Some other value',
        showPersons: true
    };

    switchNameHandler = (event, id) => {
        const personIndex = this.state.persons.findIndex(p => {
            return p.id === id;
        });

        const person = {...this.state.persons[personIndex]};
        person.name = event.target.value;

        const persons = [...this.state.persons];
        persons[personIndex] = person;
        this.setState({
            persons: persons
        })
    };
    tooglePersons = () => {
        const show = this.state.showPersons;
        this.setState({showPersons: !show});
    };

    deletePersonHandler = (personIndex) => {
        const persons = this.state.persons;
        persons.splice(personIndex, 1);
        this.setState({persons: persons});
    };

    render() {

        const style = {
            backgroundColor: 'green',
            color: 'white',
            font: 'inherit',
            border: '1px solid blue',
            padding: '8px',
            cursor: 'pointer',
            ':hover': {
                backgroundColor: 'lightgreen',
                color: 'black'
            }
        };

        let persons = null;
        if (this.state.showPersons) {

            persons = (
                <div>
                    {
                        this.state.persons.map((person, index) => {
                            return <Person
                                click={() => this.deletePersonHandler(index)}
                                name={person.name}
                                age={person.age}
                                key={person.id}
                                changed={(event) => this.switchNameHandler(event, person.id)}/>
                        })
                    }
                </div>
            );
        }

        return (
            <div className={classes.App}>
                <h1>hi, I'm react app</h1>
                <p >This is really working!</p>
                <button style={style} onClick={this.tooglePersons}>Switch Name</button>
                {persons}
            </div>
        );
    }
}

export default App;
